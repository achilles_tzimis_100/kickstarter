# -*- coding: utf-8 -*-
"""
Created on Thu Nov  2 20:32:21 2017

@author: linkw
"""
from sklearn.preprocessing import LabelEncoder
import pandas as pd
import numpy as np
import pickle
from xgboost import XGBClassifier
from sklearn.metrics import accuracy_score

import nltk
nltk.download("stopwords")
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer
stopwords=stopwords.words('english')
stemmer=SnowballStemmer('english')
import re

r=re.compile(r'[\W]', re.U)
print("before")



f = open('varlist.pckl','rb')
var_list = pickle.load(f)
f.close()


labelencoder_1 =var_list[0]
labelencoder_2 = var_list[1]
onehotencoder = var_list[2]
cvt = var_list[3]
tf = var_list[4]
tsvd = var_list[5]
nrm = var_list[6]
km= var_list[7]
onehotencoder2= var_list[8]
xgb = var_list[9]


###########################################################################################
#predict the test dataset for submission.
#test_data=pd.read_csv("D:\\Datasets\\kickstarter\\test.csv")
print("predict the test dataset for submission.")

import sys
if sys.version_info[0] < 3: 
    from StringIO import StringIO
else:
    from io import StringIO
TESTDATA = StringIO("""project_id;name;desc;goal;keywords;disable_communication;country;currency;deadline;state_changed_at;created_at;launched_at
    kkst917493670;Bràthair.;"My first film, of many to come. Trying to pursue my passion, student filmmaker, Disneyland employee, full time dreamer.";7000;brathair;false;US;USD;1449619185;1449619185;1446002581;1446159585
    """)
test_data=pd.read_csv(TESTDATA,sep=";")
print(test_data)
test_data.iloc[:,2].replace(np.NAN, '---', inplace=True)
test_data.iloc[:,4].replace(np.NAN, '---', inplace=True)
test_data.iloc[:,2] = test_data.iloc[:,2] + " " + test_data.iloc[:,4]
test_data.iloc[:,6] = test_data.iloc[:,6].map(lambda x: 'unknown' if x not in labelencoder_2.classes_ else x)
test_data.iloc[:,2]=test_data.iloc[:,2].apply(lambda x : ' '.join(stemmer.stem(w) for w in re.sub('[\\s]+',' ',r.sub(' ',x.lower())).split() if w not in stopwords))
test_data=test_data.assign(txt_len=test_data.iloc[:,2].apply(lambda x : len(x.split())).values)
txt_data=test_data.iloc[:,2].values[...,None]
pred_x=test_data.iloc[:,[3,5,6,8,10,11,12]].values
launch_to_deadline=np.subtract(pred_x[:,3],pred_x[:,5])[...,None]
launch_to_deadline=np.divide(launch_to_deadline,86400)
creation_to_deadline=np.subtract(pred_x[:,3],pred_x[:,4])[...,None]
creation_to_deadline=np.divide(creation_to_deadline,86400)
creation_to_launch=np.subtract(pred_x[:,5],pred_x[:,4])[...,None]
creation_to_launch=np.divide(creation_to_launch,86400)
pred_x=np.concatenate((pred_x,launch_to_deadline,creation_to_deadline,creation_to_launch),1)
pred_x=np.delete(pred_x, 3,1)
pred_x=np.delete(pred_x, 3,1)
pred_x=np.delete(pred_x, 3,1)
pred_x[:,1]=labelencoder_1.transform(pred_x[:,1])
pred_x[:,2]=labelencoder_2.transform(pred_x[:,2])
pred_x=onehotencoder.transform(pred_x).toarray()
pred_x=np.concatenate((pred_x,txt_data),1)
txt2=pred_x[:,19]
pred_x=np.delete(pred_x,19,1)
text_features2 = cvt.transform(txt2)
text_features2= tf.transform(text_features2)
text_features2=tsvd.transform(text_features2)
text_features2=nrm.transform(text_features2)
cluster_test=km.predict(text_features2)[...,None]

#1h clusters for use with XGB
cluster_test=onehotencoder2.transform(cluster_test).toarray()

pred_x=np.concatenate((pred_x,cluster_test,text_features2),1)
#predictions = rf.predict(pred_x)
predictions = xgb.predict(pred_x)
predictions=[round(k) for k in predictions]

#save csv for upload
test_data=test_data.assign(final_status=predictions)
sub=test_data.iloc[:,[0,13]]
#sub.to_csv("D:\\Datasets\\kickstarter\\subrf.csv",index=False)
print("save csv")
sub.to_csv("subxgb_load.csv",index=False)

#cleanup
del txt_data,creation_to_deadline,creation_to_launch,launch_to_deadline,txt2, text_features2,cluster_test,pred_x,predictions,test_data,sub
